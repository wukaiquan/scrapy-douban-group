# -*- coding: utf-8 -*-
import scrapy
from douban_group.items import TumblrGirlsItem
import scrapy
from scrapy.http import Request
#from scrapy import log
import os

# 创建文件夹
def createFile(file_path):
    if os.path.exists(file_path) is False:
        os.makedirs(file_path)
    # 切换路径至上面创建的文件夹
    os.chdir(file_path)

filePath = "E:/demo"
fileName = "distinct.txt"
class DownloadSpider(scrapy.Spider):
    name = "download"
    allowed_domains = ["tumblr.com"]
    createFile(filePath)
    fileName = filePath + "\\"+fileName
    start_urls = [ i.strip() for i in open(fileName).readlines() ]

    def start_requests(self):
        for url in self.start_urls:
            filename = url[url.rfind('/')+1:]
            filepath = "/mnt/F/Src/girls/" + filename
            if os.path.exists(filepath):
                #log.msg("file:[" + filename + "] exists. skipped.")
                print("file:[" + filename + "] exists. skipped.")
                continue
            else:
                yield self.make_requests_from_url(url)

    def parse(self, response):
        item = TumblrGirlsItem()
        filename = response.url[response.url.rfind('/')+1:]
        filepath = "/mnt/F/Src/girls/" + filename
        if os.path.exists(filepath):
            #log.msg("################# file exists #######################")
            print("################# file exists #######################")
        fp = open(filepath, "wb")
        fp.write(response.body)
        fp.flush()
        item["file_path"] = filepath
        yield item
